(ns day03
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]))




(def sample "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..")

;; (u/download-data 2023 03)
(def raw-data (slurp "data/day03.dat"))

(defn is-digit? [c] (<= (int \0) (int c) (int \9)))
(def not-symbols #{\0 \1 \2 \3 \4 \5 \6 \7 \8 \9 \.})

(def data (str/split-lines raw-data))
(def sample (str/split-lines sample))

(defn get-stars [data]
  (for [r (range (count data))
      c (range (count (get  data 0)))
        :when (= (get-in data [r c]) \*)
        ]
    [r c]
    ))


(defn get-symbols [d]
  (for [r (range 0   (count d))
        c (range  0 (count (first d)))
        :let [item (get-in d [r c])]
        :when (not (contains? not-symbols item) )

        ]
       [r c]
       ))

(count  (first (map vec sample)))
(get-in (mapv vec sample) [2 3])
(get-symbols data)
(count  (get-symbols  data))

(defn get-parts-from-line [row line]
  (let [ matches (re-seq #"\d+" line) ]
    (loop [matches matches
           current (first matches)
           index 0
           result []
           ]
      (if (nil? current)
        result
        (recur (rest matches)
               (first  (rest matches))
               (+ index 1);;(count current))
               (conj result {:num (edn/read-string current) :row row :col (str/index-of line current (dec index))
                                :len (count current)
                                
                             })
               )))))

(defn get-parts [data]
  (map-indexed (fn [idx l]
                 (get-parts-from-line idx l)
                 )
               data))

(defn parts-to-dict [parts]
  (reduce (fn [d {:keys [num row col len id]}]
            (assoc d id {:id id :row row :col col :num num :len len}))
          (sorted-map) parts))


(def parts (parts-to-dict (map-indexed (fn [idx d]
                (assoc d :id idx))
                 (flatten (get-parts data)))))

;;(def parts (parts-to-dict (mapv identity (flatten(get-parts data)))) 
(def stars (get-stars data))
(def symbols (get-symbols data))
(count parts);; 1912
(count symbols) ;; 16152 140x140
(get-symbols data)
symbols

(def parts (parts-to-dict (map-indexed (fn [idx data]
                (assoc data :id idx))
                 (flatten (get-parts sample)))))
(def stars (get-stars sample))
(def symbols (get-symbols sample))
parts
stars
symbols 
(count parts)
(range 0 3)

(defn part-adjacent-symbol [{:keys [row col num len]} symbols]
  (let [border (for [r (range (dec row) (+ row 2))
                     c (range  (dec col) (+ col len 1  ))
                     ]
                 
                 [r c])
        bset (into #{} border)
        sset (into #{} symbols)
        res     (set/intersection bset sset)
        
        ]
    (if (not (empty?   res))
      num
0      )))

(first #{[2 3]})

 (map #(part-adjacent-symbol (get parts %) symbols) (sort (keys  parts)))
;;517021

(reduce + (map #(part-adjacent-symbol (get parts %) symbols) (sort (keys  parts))))


(ns utils
  (:require [clojure.java.io :as io]
            [clj-http.client :as client]
            [hashp.core ]
            [clojure.edn :as edn]))

(defn download-data
  "if file doesn't already exist, download the data and store in
  data/day#.dat"
  [year day]
  (let [fname (format "day%02d.dat" day)
        url (str "https://adventofcode.com/" year "/day/" day "/input")
        cookie "session=53616c7465645f5fd459afd5fd38064616059a49ef7f3073b9880ffc77754861018cb4be3fc007e9161a5347572d3192f2de781aae394be78902a9d9634d5685"
        outfile (format "data/%s" fname)
        ]
    (if (not (.exists (io/file outfile)))
      (let [data (:body (client/get url {:headers {:Cookie cookie}}))]
        (spit outfile data))
      "File already exists"
      )))
    



(defn load-data [year day]
  (download-data year day)
  (let [fname (format "day%02d.dat" day)]
  (slurp (str "data/" fname))))

(defn transpose [m]
  (apply mapv vector m))

(defn char->int [c]
  (Long/parseLong (String/valueOf c)))

(defn gcd [a b]
  (if (zero? b)
    a
    (recur b (mod a b))))

(defn lcm [a b]
  (/ (abs (* a b))
     (gcd a b)))

(defn manhattan-distance [[x1 y1] [x2 y2]]
  (+ (abs (- x2 x1 )) (abs (- y2 y1))))

(defn get-perimeter
  "assumes a closed list of vertices"
   [vertices]

  (reduce + (map manhattan-distance vertices (rest vertices))))

(defn get-area
  "shoelace theorem"
  [points]
  
  (let [sum-diagonals (fn [sum [[x1 y1] [x2 y2]]]
                        (+ sum (- (* x1 y2) (* x2 y1))))]
    (abs (/ (reduce sum-diagonals 0 (partition 2 1 points)) 2))))



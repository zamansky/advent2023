(ns day25
    (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math :as m]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   [loom.graph :as g]
   [loom.alg :as alg]
   [loom.io :as vg]
   ))

;; (u/load-data 2023 25)

(def data (slurp "data/day25.dat"))

;; Removed this links from data file
;; used this to find links: dot -Tsvg z3.dot -o z3.svg -Kneato
;; lxb-vcq
;; ddj-rnx
;; mmr-znk

(def sample
  "jqt: rhn xhk
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr
")
(defn parse-line [graph line]
  (let [[node & neighbors] (re-seq #"[a-z]{3}" line)]
    (reduce (fn [g next]
              (-> (update g node (partial conj) next)
                  (update next (partial conj) node))
              ) graph neighbors)
))

         
(defn parse [data]
  (let [lines (str/split-lines data)]
    (reduce (fn [g next]
              (parse-line g next))
            {} lines)))


(def adj-list (parse data))
(def g (g/graph adj-list))

(* (count (alg/bf-traverse g "lxb"))

(count (alg/bf-traverse g "vcq"))
)

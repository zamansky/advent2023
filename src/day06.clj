(ns day06
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt]]
   ))

(def sample "Time:      7  15   30
Distance:  9  40  200")



;; (u/load-data 2023 06)
(def data (slurp "data/day06.dat"))


(defn parse [data]
  (let [x (->> (str/split-lines data)
              (mapv #(str/split % #": "))
              (mapv (partial drop 1))
              (mapv first)
              (mapv str/trim)
              (mapv #(str/split % #" +"))
              (mapv #(mapv parse-long %)))
       ]
    (map list (first x) (second x))
  ))

(def x (parse sample))

(defn dist [press time]
  (* press (- time press)))

(defn get-winners [[time record]]
  (->> (for [press (range (inc time))]
      (dist press time))
     (filter #(< record %) )
     count)
  )

(apply * (map get-winners (parse data)))

(defn parse2 [data]
  (let [x (->> (str/split-lines data)
              (mapv #(str/split % #": "))
              (mapv (partial drop 1))
              (mapv first)
              (mapv #(str/replace % " " ""))
              (mapv parse-long)
       )]
    ;;   (map list (first x) (second x))
    x
  ))

(get-winners (parse2 data))



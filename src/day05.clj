(ns day05
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt]]
   ))

(defn parse-longs [s]
  (map parse-long (re-seq #"\d+" s)))

;;(def raw-sample (slurp "./data/sample05.dat"))
;; (u/load-data 2023 05)
;;(def raw-data (slurp "data/day05.dat"))

(def raw-data (slurp "/home/zamansky/gh/advent2023/data/day05.dat"))

;;(first (str/split raw-sample #"\n\n")) 
;;(def r (rest (str/split raw-sample #"\n\n")) )
;;(def x (first (rest (str/split raw-sample #"\n\n"))))

(defn part1-parse [data]
   (->> data
     (str/split-lines)
     (partition-by empty?)
     (take-nth 2)
     ))

(defn part1-parse-maps [maps]
  (map #(map parse-longs (drop 1 %)) maps))



(def maps (part1-parse-maps (drop 1 (part1-parse raw-data))))
(def seeds (map parse-long (drop 1(str/split (first  (first (part1-parse raw-data))) #" "))))

;;(def maps (part1-parse-maps (drop 1 (part1-parse raw-sample))))
;;(def seeds (map parse-long (drop 1(str/split (first  (first (part1-parse raw-sample))) #" "))))

(defn convert-seed [seed [dst src size]]
  (if (<= src  seed  (+ src size))
    (- seed (- src dst))
    seed
    ))

(defn convert-seed-all-maps [seed maps]
  (reduce (fn [seed map]
            (let [newseed (convert-seed seed map)]
              (if (not= seed newseed)
                (reduced newseed)
                seed))) seed maps))

;;(def seeds [0 1 48 49 50 51 96 97 98 99])

(defn do-all-seeds-one-map [seeds map]
  (map #(convert-seed-all-maps % map) seeds)
  )

;;(do-all-seeds-one-map seeds  (first maps))

(defn part1 [seeds maps]
(apply min (reduce (fn [seeds m]
          (map #(convert-seed-all-maps % m) seeds)
                     ) seeds maps)))

;;(part1 seeds maps)
;; seed -> all the


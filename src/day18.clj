(ns day18
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math :as m]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 18)
(def data (slurp "data/day18.dat"))

(def sample
  "R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)")


(def dirs {"U" [-1 0]
           "D" [1 0]
           "L" [0 -1]
           "R" [0 1]})

(defn get-vertices [data]
  (reductions (fn [accum [dir num _]]
                (map + accum (map * (dirs dir) #p (repeat 2 num)))
                )[0 0] data))



(defn find-trench
  "Uses picks theorem after area uses shoelace"
   [vertices]
  (let [perimeter (u/get-perimeter vertices)
        area (u/get-area vertices)
        ]
    (inc (+ area (/ perimeter 2)))))


(defn parse [data]
  (->> data
      (re-seq #"([UDLR]) (\d+) \(#([a-f0-9]{6})\)" )
      (map (fn [[_ dir num color]] [dir (parse-long num) color]))
      ))

(defn part1 [data]
  (let [vertices (get-vertices (parse data))]
    (find-trench vertices)))


(defn parse-hex [hex]
  (let [dirs {\0 "R" \1 "D" \2 "L" \3 "U"}
        dist  (edn/read-string  (apply str "0x"  (take 5 hex)))
        dir (dirs (last hex))]
    [dir dist hex]))

(defn parse-part2 [data]
  (->> data
       parse
       (map #(parse-hex (last %)))
       ))

(defn part2 [data]
  (let [vertices (get-vertices (parse-part2 data))]
    (find-trench vertices)))

(part2 data)



(ns day10
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [loom.graph :as g]
   [loom.alg :as alg]
   [loom.io :as vg]
   ))

;; (u/load-data 2023 10)
(def raw-data (slurp "data/day10.dat"))

(def sample "-L|F7
7S-7|
L|7||
-L-J|
L|-JF")

(def sample2 "..F7.
.FJ|.
SJ.L7
|F--J
LJ...")

(def sample3 "...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........")

(def pipe->deltas
  {\| [[-1 0] [1 0]]
   \- [[0 -1] [0 1]]
   \L [[-1 0] [0 1]]
   \J [[-1 0] [0 -1]]
   \7 [[1 0][0 -1]]
   \F [[1 0] [0 1]]
   \S [[1 0][0 -1]] ;; for sample and sample2 [1 0] [0 1] data [1 ]] [0 -1]
   \. []
   })

(defn make-graph [data]
  (let [lines data
        rows  (count lines)
        cols  (count (first lines))
        
        ]
    (into {} (for [row (range rows)
          col (range cols)
          :let [currentchar (get-in lines [row col])
                neighbors  (mapv #(mapv + [row col] %) (pipe->deltas currentchar))]]
          {[row col] neighbors}
          ))))
        


(defn find-start [data]
  (for [row (range (count data))
        col (range (count (first data)))
        :when (= \S (get-in data [row col]) )
        ]
    [row col]))


(defn bfs [graph start]
  (loop [frontier [start]
         solset []
         step 0
         ]
    (let [current (first frontier)
          neighbors  (get graph current)
          for-new-front (filter #(not ((set solset) %))neighbors)
          newfront  (into [] (concat (rest frontier) for-new-front))
          newsol (concat solset [current])
          ]
      (if (empty? newfront)
        [step newsol]
        (recur newfront newsol (inc step))))))




(defn part1 [raw-data]
  (let [data (str/split-lines raw-data)
        g (make-graph data)
        s (find-start data)
        ]
    (/ (first (bfs g (first s))) 2)))

;; (part1 raw-data)
(defn get-vertices [raw-data]
  (let [data (str/split-lines raw-data)
        g (make-graph data)
        s (find-start data)
        ]
    (second (bfs g (first s))) ))


;; shoelace theorem 
;;(def vertices (filter #(not (#{\| \- \.} %)) nodes))

;; #{\| \- \L \J \7 \F \S}
(defn process-line [row line nodes node-set]
  (loop [c 0
         inside false
         result 0
         ]
    (let [newinside (if (and (node-set [row c])
                             (not= \- (get line c))
                             ( and (not (node-set [row (dec c)]))
                             (not (#{\-} (node-set [row (dec c)]) ))))
                      (not inside)
                      inside)
          newres (if (and inside (not (node-set [row c])))
                   (inc result)
                   result)
          
          ]
      (if (= c  (count line))
        result
        (recur (inc c) newinside newres))
      )))



;; 393

(defn part2 [raw-data] 
  (let [graph (str/split-lines raw-data)
       nodes (get-vertices raw-data)
       node-set (into #{} nodes)
       ]
    (loop [row 0
           result 0
           ]
      (if (>= row  (count graph))
        result 
        (let [line (get graph row)
              newresult (+ result (process-line row line nodes node-set))
              ]
          (recur (inc row) newresult)
          )))))

;; (part2 raw-data)

(defn dfs [graph start]
  (loop [frontier [start]
         solset []
         step 0
         ]
    (let [current (first frontier)
          neighbors  (get graph current)
          for-new-front (filter #(not ((set solset) %))neighbors)
          newfront  (into [] (concat for-new-front (rest frontier) ))
          newsol (concat solset [current])
          ]
      (if (empty? newfront)
        [step newsol]
        (recur newfront newsol (inc step))))))


(def g (make-graph (str/split-lines raw-data)))
(def s (find-start (str/split-lines raw-data)))
(def path-nodes (second (dfs g (first s))))
(def array (str/split-lines raw-data))
(def vertices (filter (fn [x] (not (#{\-\} (get-in array x)) )) path-nodes)))

(def xs (map first vertices))
(def ys (map second vertices))

(def xs (concat xs (last xs)))
(def ys (concat ys (last ys)))
  
(count xs)
(count ys)

;; (/ (- (apply + (map * (drop-last xs) (drop 1 ys)))
;;       (apply + (map * (drop-last ys) (drop 1 xs)))) 2)
;;   (map + [1 2] [10 20])

;; makea 2d board  of X's and spaces to manually
;; figure out part 2
;;
;; (def board (vec (map vec  (str/split-lines raw-data))))

;; (def clean-board 
;; (reduce (fn [board [row col] ]
;;           (if (not (node-set [ row  col]))
;;             (assoc-in board [row col] \space )
;;             (assoc-in board [row col] \X) ))
;;         board (for [x (range 140) y (range 140)]
;;                 [x y])))


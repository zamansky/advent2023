(ns day16
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math :as m]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 16)
(def data (slurp "data/day16.dat"))

(def sample (slurp "data/sample16.dat"))

(defn parse [data]
  (into {} (let [lines (str/split-lines data)]
    (for [row (range (count lines)) col (range(count (first lines)))]
      {[row col] (get-in lines [row col])}
      ))))

(def sample-board (parse sample))
(def board (parse data))

(def up [-1 0])
(def down [1 0])
(def left [0 -1])
(def right [0 1])

(def slash-map {up right down left left down right up}) ; /
(def bslash-map {up left down right left up right down}) ; \

(defn move [loc delta] [(mapv + loc delta) delta])


(defn update-ray [board loc delta]
  (filter #(board (first %)) (let [target (board loc)]
                               (cond
                                 (= target \.) [(move loc delta)]
                                 (= target \/) [(move loc (slash-map delta))]
                                 (= target \\) [(move loc (bslash-map delta))]
                                 (= target \-) (if (#{left right} delta)
                                                 [(move loc delta)]
                                                 [(move loc left) (move loc right)])
                                 (= target \|) (if (#{up down} delta)
                                                 [(move loc delta)]
                                                 [(move loc up) (move loc down)])
                                 ))))

;; (defn trace-rays [board start delta]
;;   (loop [rays [[start delta]] solset #{}]
;;     (if-some [[l d :as ray] (first rays)]
;;       (if (solset ray)
;;         (recur (rest rays) solset)
;;         (recur (apply conj (rest rays) (update-ray board l d)) (conj solset ray)))
;;       (->> solset (map first) set count)

;;       )))


(defn trace-rays [board start delta]
  (loop [ [[l d :as ray] & rays] [[start delta]] solset #{}]
    (if (nil? ray)
      (->> solset (map first) set count)
      (if (solset ray)
        (recur rays solset)
        (recur (apply conj  rays (update-ray board l d)) (conj solset ray))))))



(count (set (map first (trace-rays board [0 0] right))))

(defn part1 [data]
  (let [board (parse data)]
    (trace-rays board [0 0] right)))

(part1 data) ;; 7111

(defn part2 [data]
  (let [board (parse data)
        lines (str/split-lines data)
        rows (count lines)
        cols (count (first lines))]
    (apply max [(apply max (for [row (range rows)] (trace-rays board [row 0] right)))
                (apply max (for [row (range rows)] (trace-rays board [row cols] left)))
                (apply max (for [col (range cols)] (trace-rays board [0 col] down)))
                (apply max (for [col (range cols)] (trace-rays board [rows col] up)))])
    ))


(part2 data)

(defn p2 [data]
  (let [board (parse data)
        lines (str/split-lines data)
        rows (count lines)
        cols (count (first lines))
        rrows (range rows)
        rcols (range cols)]
    (apply max [(apply max (pmap #(trace-rays board [% 0] right)  rrows ) ) ;; row 0
                (apply max (pmap #(trace-rays board [% cols] left) rrows)) ;; row cols
                (apply max (pmap #(trace-rays board [0 %] down)  rcols)) ;; 0 col
                (apply max (pmap #(trace-rays board [rows %] up) rcols)) ;; 0 cols 
                ])
    ))


(time (part2 data))
(p2 data)

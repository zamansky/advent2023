(ns day24
    (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math :as m]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 24) 

(def data (slurp "data/day24.dat"))

(def sample
  "19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3")

(def data (partition 6 (map parse-long (re-seq #"\d+" sample))))

(defn parse-eqn [[x y _z dx dy _dz] ]
  (let [slope (/ dy dx)]
    [slope (- y (* x slope))]))

(defn intersection [p1 p2]

  )
(parse-eqn (first data))
(map parse-eqn data)
(comment
y =- mx + b

)

(ns day01
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]))


(def sample "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet")

(def sample2 "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen")

;; (u/download-data 2023 01)
(def raw-data (slurp "data/day01.dat"))

(defn digit? [c] (and (>= 1 (compare \1 c)) 
                      (>= 1 (compare c \9))))
(defn part1 [input]
  (->> (str/split input #"\n")
       (map #(filter  digit? %) )
       (map #(str (first %) (last %)))
       (map u/parse-int)
       (apply +)
))


(part1 raw-data)
  
(def table {"one"  "1"
            "two"  "2"
            "three"  "3"
            "four"  "4"
            "five"  "5"
            "six"  "6"
            "seven"  "7"
            "eight"  "8"
            "nine"  "9"
            })

(def part1-regex #"(\d)")
(def part2-regex #"(?=(one|two|three|four|five|six|seven|eight|nine|\d))")



(defn convert-line [line regex]
  (->> (mapv second (re-seq regex line))
      (map #(if (table %) (table %) %))
      ))


(defn solve [data regex]
  (let [lines (str/split-lines data)]
    (->> (map #(convert-line % regex) lines)
       (map #(str (first %) (last %)))
       (map u/parse-int)
       (apply +)
       )))


(solve raw-data part1-regex)
(solve raw-data part2-regex)


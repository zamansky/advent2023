(ns day14
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math :as m]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 14)
(def raw-data (slurp "data/day14.dat"))

(def raw-sample
  "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....")

(def sample (str/split-lines raw-sample))
(def data (str/split-lines raw-data))

(defn scan-back [board row col]
  (loop [cur (dec row)]
    (cond
      (< cur 0) (inc cur)
      (or (= (get-in board [cur col]) \O)
          (= (get-in board [cur col]) \#)) (inc cur)
      :else (recur (dec cur)))))
      
(defn tilt-board [board col]
  (loop [board board
         src 1
         ]
    (cond
      (>= src (count board)) board
      
      (= (get-in board [src col]) \O)
      (let [dst  (scan-back board src col)
            newboard (-> (assoc-in board [src col] \.)
                         (assoc-in  [dst col] \O))]
        (recur  newboard (inc src)))

      :else (recur board (inc src)))))

(defn score [board]
  (let [rows (count board)]
    (for [row (range rows)]
      (let [r (get board row)
            rocks (count  (filter #(= % \O) r))
            linescore (* (- rows row) rocks)
            ]
         linescore))))
        

(def board (vec (map vec sample)))
(def dboard (vec (map vec data)))

(def tilted (reduce (fn [b i] (tilt-board b i)) board (range  (count board))))

(defn part1 [data]
  (let [board (vec (->> (str/split-lines data)
                   (map vec)
                   ))
        tilted (reduce (fn [b i] (tilt-board b i)) board (range  (count board)))
        ]
    (apply + (score tilted))))
    
    

(part1 raw-sample)
(part1 raw-data)

(defn tilt [board]
  (reduce (fn [b i] (tilt-board b i)) board (range  (count board))))

(defn rotate-board [board]
  (mapv (comp vec reverse) (u/transpose board)))


(def spin (comp rotate-board tilt rotate-board tilt
                rotate-board tilt rotate-board tilt))

(defn find-cycle [board cycles]
  (loop [b board
         i 0
         index->board {}
         board->index {}]
    (cond (= i cycles) b

          (contains? board->index  b)
          (let [last-index (board->index b)]
                (index->board (+ last-index
                                   (rem (- cycles i)
                                        (- i last-index)))))
          :else
          (recur (spin b) (inc i)
                 (assoc index->board i b)
                 (assoc board->index b i )))))
                  
;; part2 (apply + (score (find-cycle dboard 1000000000 )))

(loop [ [x & xs] [1 2 3 4 5]]
  (if (empty? #p xs)
    x
    (recur xs)))

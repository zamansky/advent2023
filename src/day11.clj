(ns day11
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 11)
(def data (str/split-lines (slurp "data/day11.dat")))

(def sample (str/split-lines "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
"))


(defn expand-galaxies [g]
  (loop [lines g
         result []
         ]
    (if (empty? lines)
      result
      (let [line (first lines)
            next-lines (rest lines)
            result (if (every? #{\.} line)
                     (conj result line line)
                     (conj result line))
            ]
        (recur next-lines result)))))


(defn extract-galaxies [g]
  (->> (for [row (range (count g))
             col (range (count (first g)))
             :when (= \#  (get-in g [row col]))]
         [row col])
       (map-indexed (fn [i x] [i x]))
       (into {} )
       ))

(defn dist [[[a1 a2] [b1 b2]]]
  (+ (abs (- a1 b1)) (abs (- a2 b2))))

(defn part1 [data]
  (let [galaxy-map (-> data expand-galaxies u/transpose
                     expand-galaxies u/transpose)
        galaxies (extract-galaxies galaxy-map)
        ]
        (apply + (map dist   (comb/combinations (vals galaxies) 2)))
    ))

(defn calc-spans [galaxy-map span]
  (->>
   (loop [g galaxy-map
          result []
          ]
     (if (empty? g) result
         (let [l (first g)
               r (rest g)
               v (if  (every? #{\.} l)
                   span 1)
               newres (conj result v)
               ]
           (recur r newres))))
   (map-indexed (fn [i x] [i x]))
   (into (sorted-map))
   ))


(defn dist-part2 [[[r1 c1][r2 c2]]]
  (let [row-range (apply range (sort [r1 r2]))
        col-range (apply range (sort [c1 c2]))
        rowsum (apply + (map vert-spans row-range))
        colsum (apply + (map horz-spans col-range))
        ]
    (apply + [rowsum colsum])
    ))

(defn part2 [data]
  (let [vert-spans (calc-spans data 1000000)
        horz-spans (calc-spans (u/transpose data) 1000000)
        galaxies (extract-galaxies data)
        ]
    (apply + (map dist-part2 (comb/combinations (vals galaxies) 2)))))

(part2 data)


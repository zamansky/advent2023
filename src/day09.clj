(ns day09
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   ))

;; (u/load-data 2023 9)
(def data (slurp "data/day09.dat"))

(def sample "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45")

(defn parse [data]
  (->> data
       str/split-lines
       (map #(str/split % #" "))
       (map #(map parse-long %))
       )
  )

(parse sample)

(defn process-line-recursive [line]
     (if (every? zero? line)
       0
       (+ (last line)
          (process-line-recursive  (map (fn [[a b]] (- b a))  (partition 2 1 line))))))

(defn expand-line [line]
  (map (fn[[a b]] (- b a)) (partition 2 1 line) ))

(defn process-line [line]
  (loop [line line
         sum 0]
    (if (every? zero? line)
      sum
      (recur (expand-line line)
             (+ sum (last line))))))

(defn part1 [data]
  (apply + (map process-line  (parse data))))

(part1 data)

(defn part2 [data]
  (apply + (map process-line (map reverse (parse data)))))

(part2 data)

(transduce  (comp (map reverse) (map process-line)) +  (parse data))


(defn part2-recursive [line]
     (if (every? zero? line)
       (first line)
       (- (first line)
          (part2-recursive  (map (fn [[a b]] (- b a))  (partition 2 1 line))))))

(apply + (map part2-recursive (parse data)))


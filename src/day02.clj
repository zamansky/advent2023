(ns day02
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]))


(def sample "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green")


(defn draw-valid? [draw]
  (let [maxs {"red" 12 "green" 13 "blue" 14}]
  (and (<= (get draw "red") (get maxs "red"))
       (<= (get draw "green") (get maxs "green"))
       (<= (get draw "blue") (get maxs "blue")))))

(defn draw-to-dict [draw]
  (reduce (fn [d next]
            (let [[amt color] (str/split next #" ")]
            (assoc d color (u/parse-int amt))
            )) {"red" 0 "green" 0 "blue" 0} (str/split draw #", ")))

(defn parse-line-part1 [line]
  (let [ [_ id draws] (re-find #"Game (\d+): (.*)" line)
        draws (map draw-to-dict (str/split draws #"; "))]
    (if (every? identity  (map draw-valid? draws))
      (u/parse-int id) 0)
      ))


(defn part1 [data]
  (apply + (map parse-line-part1 (str/split-lines data))))

(part1 data)

(defn get-max [draws color]
  (apply max(map #(get  % color) draws)
  ))

(defn parse-line-part2 [line]
  (let [ [_ id draws] (re-find #"Game (\d+): (.*)" line)
        draws (map draw-to-dict (str/split draws #"; "))
        maxreds (get-max draws "red")
        maxgreens (get-max draws "green")
        maxblues (get-max draws "blue")
        ]
    (* maxreds maxgreens maxblues)
      ))

(defn part2 [data]
  (apply +(map parse-line-part2 (str/split-lines data))))

(part2 data)


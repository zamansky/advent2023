(ns day15
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math :as m]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 15)
(def data (slurp "data/day15.dat"))

(def sample "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7")

(defn calc-hash [word]
  (loop [ [x & xs]  word hash 0]
    (if (nil? x)
      hash
      (recur xs (rem (* (+ (int x) hash) 17) 256)))))

(defn part1 [data]
  (apply + (map calc-hash (str/split data #","))))

;;----------------------------------------


(defn parse-op [line]
  "hash label value op"
  (let [[label val] (str/split line  #"=|-")
        op (re-find #"=|-" line)]
    [(calc-hash label) label val op]
    ))


(defn update-row [row label val]
  (if (some #(= (first %) label) row)
    (mapv #(if (= (first %) label)
             [label val]
             %) row)
    (concat  row [[label val]])
  ))
(defn add-to-table [table h label val]
  (let [row (table h)]
    (assoc table h (update-row row label val))
   ))


(defn rm-from-table [table h label]
  (let [row (table h)]
    (assoc table h (filter #(not= (first %) label) row)
           )))

(defn do-op [table line]
  (let [ [h label val op] (parse-op line)]
    (if (= op "=")
      (add-to-table table h label val)
      (rm-from-table table h label))))

(defn part2-score [table]
  (for [item table
        :when (not (empty? (second item)))]
    (let [box (inc (first item))
          slots (second item)
          ]
      (apply + (map-indexed (fn [idx item]
                     (* box
                      (inc idx)
                        (parse-long (second item))
                        )) slots )
         ))
      ))


(defn part2 [data]
  (let [hashmap  (zipmap (range 256) (repeat []))
        res (reduce (fn [table next]
                      (do-op table next)) hashmap (str/split data #","))]
    (apply + (part2-score res))))


(part2  data)



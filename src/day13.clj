(ns day13
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 13)
(def raw-data (slurp "data/day13.dat"))


(def sample
  "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#"
  )

(def boards
  (->> (str/split raw-data #"\n\n")
      (map str/split-lines)
       )

  )

(def samples
  (->> (str/split sample #"\n\n")
      (map str/split-lines)
       )

  )


(defn is-reflected? [a i]
  (let [[l r] (split-at i a)
        diffs (map =  (reverse l) r)
        ]
(every? true? diffs)))

(defn find-sym [board]
 (->> (count board)
       (range 1)
       (map (partial is-reflected? board))
       (keep-indexed #(if %2 %1))
       first
       ))

(defn calc-score [board]
  (let [vindex  (find-sym board)
        hindex  (find-sym (u/transpose board))
        ]
    (if (nil? hindex)
      (* 100 (inc vindex))
      (inc hindex))))
                         
(defn part1 [boards]
  (apply +(map calc-score boards)))



;; part2 - count diffs for each line ?


(defn count-diffs [x y]
  (let [one (map #(map = %1 %2) x y)
        two (map #(filter false? %)one)
        three (map count two)
        ]
    (apply + three)
    ))


(defn p2-is-reflected? [a i]
  (let [[l r] (split-at i a)
        rl (reverse l)
        diffs (count-diffs rl r)
        
        ]
    diffs
    ))

(p2-is-reflected?  (first samples) 3)


(defn find-smudge [board]
 (->> (count board)
       (range 1)
       (map (partial p2-is-reflected? board))
       (keep-indexed #(if (= %2 1) %1))
       first
       
       ))
(defn calc-score-2 [board]
  (let [vindex  (find-smudge board)
        hindex  (find-smudge (u/transpose board))
        ]
    (if (nil? hindex)
      (* 100 (inc vindex))
      (inc hindex))))


(defn part2 [board]
  (apply + (map calc-score-2 boards)))

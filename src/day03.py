

sample="""467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
"""

data = open("../data/day03.dat").read()

def build_board(data):
    b=data.splitlines();
    w = len(b[0])
    l='.'*(w+2)
    for r in range(0,len(b)):
        b[r]='.'+b[r]+'.'
    b=[l]+b+[l]
    return b

def extract_numbers(board):
    numbers={}
    nd=0
    id=0
    for row in range(0,len(board)):
        col = 0
        while col < len(board[0]):
            
            if (board[row][col].isdigit()):
                nd=0
                while board[row][col+nd].isdigit():
                    nd=nd+1
                number = board[row][col:col+nd]
                numbers[(row,col)]={"n":number, "loc":(row,col),"r":row,"c":col,"digits":nd}
                id=id+1
                col=col+nd
            else:
                col=col+1
            
    return numbers

def is_adjacent_symbol(number,board):
    for r in range(number["r"]-1,number["r"]+2):
        for c in range(number["c"]-1,number["c"]+number["digits"]+1):
            if not(board[r][c].isdigit()) and (board[r][c]!='.'):
                return True
    return False

b = build_board(sample)
n = extract_numbers(b)
def part1(data):
    b = build_board(data)
    n = extract_numbers(b)
    tools = [n[x]['n'] for x in n.keys() if is_adjacent_symbol(n[x],b)]
    nums = [int(x) for x in tools]
    
    return sum(nums)



def extract_stars(board):
    stars=[]
    for row in range(len(board)):
        for col in range(len(board[0])):
            if board[row][col]=='*':
                stars.append((row,col))
    return stars

def find_numbers(star,board):
    (row,col) = star
  
    n=[]
    for r in range(row-1,row+2):
        for c in range(col-1,col+2):
            if board[r][c].isdigit():
                c2=c
                while board[r][c2].isdigit():
                    c2 = c2-1
                n.append((r,c2+1))
    return list(set(n))


def getnums(starlist,numbers):
    r=[]
    for s in starlist:
        val = numbers[s]["n"]
        r.append(val)
    return r

def part2(data):
    b = build_board(data)
    n = extract_numbers(b)
    stars = extract_stars(b)
    starn = [find_numbers(x,b) for x in stars]
    binaries = [x for x in starn if len(x)==2]
    s=0
    
    for b in binaries:
        (x,y) = getnums(b,n)
        p = int(x)*int(y)
        s=s+p


    return s


# 517021
# 81296995

#part1(sample)
#part1(data)

c= 0
for line in data.splitlines():
    for cc in line:
        if not(cc.isdigit()) and (cc != ' '):
            c=c+1
print(c)

(ns day04
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt]]
   ))

(def sample
  "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11")

;; (u/load-data 2023 04)
(def data (slurp "data/day04.dat"))



(defn parse-line [line]
  (let [[_ card wpart hpart] (re-matches #"Card +(\d+):(.+)\|(.+)" line)
        winners (->> (str/split wpart #" +")
                     (map parse-long)
                     (filter (comp not nil?)))
        hand (->> (str/split hpart #" +")
                     (map parse-long)
                     (filter (comp not nil?)))

        ]
    [(parse-long card) winners hand]))
 
(defn part1 [data]
  (->>
   (str/split-lines data)
   (map parse-line)
   
   (map (fn [ [_ winners hand] ]
          (set/intersection (into #{} winners) (into #{} hand))))
   (map vec)
   (map count)
   (filter (fn [x] (not (= 0 x))))
   (map dec)
   (map (partial expt 2))
   (apply +)
   ))

(defn get-win-counts [data]
  (->>
   data
   str/split-lines
   (map parse-line)
   (map (fn [ [card winners hand] ]
          (let [wcount (count
                        (set/intersection
                         (into #{} winners)
                         (into #{} hand)))]
            [card wcount])))))

(defn update-card-counts [cards wincount index]
  (let [multiplier (get-in cards [index 2])]
    (loop [i (inc index)
           cards cards
           ]
      (if (> i (+ index wincount)) cards
          (recur (inc i)
                 (update-in cards [i 2] #(+  multiplier %)  )
                 )
          ))))

 

(defn part2 [data]
  (let [cards (get-win-counts data)
        cards (mapv #(conj % 1) (get-win-counts data))
        result (loop [card 0 cards cards [_ winners _] (get cards card) ]
                 (cond
                   (= (count cards) card ) cards
                   :else (recur (inc card)
                                (update-card-counts cards winners card)
                                (get cards (inc card))
                                )))]
    (apply + (map #(get % 2) result))))


(part1 data)
(part2 data)

(ns day12
  (:require
   [utils :as u]
   [hashp.core]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clojure.math.numeric-tower :as math :refer [expt abs]]
   [clojure.math.combinatorics :as comb]
   ))

;; (u/load-data 2023 12)
(def raw-data (slurp "data/day12.dat"))

(def raw-sample
  "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1")

(defn parse-data [data]
  (->> data
       str/split-lines
       (map #(str/split % #" "))
       (map (fn [[l c]] (let [c (map parse-long (str/split c #","))
                              ]
                          [l c])))
       ))

(def cache (atom {}))

(defn count-ans [line nums]
  (cond
    (empty? line) (if (empty? nums) 1 0)
    (empty? nums) (if ((into #{} line) \#) 0 1)
    :else
    (if (@cache [line nums])
      (@cache [line nums])
      (let [result 0
            result (if (#{\. \?} (first line))
                     (+ result (count-ans (rest line) nums))
                     result)
            result (if (#{\# \?} (first line))
                     (if
                         (and
                          (<= (first nums) (count line))
                          (and
                           (not ((into #{} (take  (first nums) line)) \.))
                           (or
                            (=  (first nums) (count line))
                            (not= (nth line  (first nums)) \#))))
                       
                       (+ result  (count-ans (drop  (inc (first nums)) line)
                                             (drop 1 nums)))
                       result)
                     result)
	    ]
      ((swap! cache assoc [line nums] result) [line nums])
        ))))

(def z (atom {[1 1]  100 [2 2] 200}))
((swap! z assoc  [1 1] 50) [1 1])

(def x (parse-data raw-sample))
(first x)
(second x)

(map (fn [[a b]] (count-ans a b)) (parse-data raw-sample));

(def p2 (memoize count-ans))
;;7195
(apply + (map (fn [[a b]] (count-ans a b)) (parse-data raw-sample)))
(apply + (map (fn [[a b]] (count-ans a b)) (parse-data raw-data)))
(count-ans (first (second x)) (second (second x)))

(apply + (map (fn [[a b]] (p2 a b)) (parse-data raw-data)))

(defn make-part2-data [[line nums]]
  (let [newline (str/join "?" (repeat 5 line))
        newnums (flatten (repeat 5 nums))
        ]
    [newline newnums]
  ))

(def x (map make-part2-data (parse-data raw-data)))
(apply + (map (fn [[a b]] (count-ans a b)) x) )

